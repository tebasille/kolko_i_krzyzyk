﻿
namespace WindowsFormsApp1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbl1o = new System.Windows.Forms.Button();
            this.lbl1x = new System.Windows.Forms.Button();
            this.lbl2o = new System.Windows.Forms.Button();
            this.lbl2x = new System.Windows.Forms.Button();
            this.lbl3o = new System.Windows.Forms.Button();
            this.lbl3x = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.lbl4o = new System.Windows.Forms.Button();
            this.lbl4x = new System.Windows.Forms.Button();
            this.lbl5o = new System.Windows.Forms.Button();
            this.lbl5x = new System.Windows.Forms.Button();
            this.lbl6o = new System.Windows.Forms.Button();
            this.lbl6x = new System.Windows.Forms.Button();
            this.lbl7o = new System.Windows.Forms.Button();
            this.lbl7x = new System.Windows.Forms.Button();
            this.lbl8o = new System.Windows.Forms.Button();
            this.lbl8x = new System.Windows.Forms.Button();
            this.lbl9o = new System.Windows.Forms.Button();
            this.lbl9x = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.gvg = new System.Windows.Forms.Button();
            this.gvk = new System.Windows.Forms.Button();
            this.label13 = new System.Windows.Forms.Label();
            this.czyszczenieWynikow = new System.Windows.Forms.Button();
            this.wyczyszcPlansze = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lbl1o
            // 
            this.lbl1o.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl1o.Location = new System.Drawing.Point(12, 12);
            this.lbl1o.Name = "lbl1o";
            this.lbl1o.Size = new System.Drawing.Size(75, 53);
            this.lbl1o.TabIndex = 0;
            this.lbl1o.Text = "o";
            this.lbl1o.UseVisualStyleBackColor = true;
            this.lbl1o.Visible = false;
            this.lbl1o.Click += new System.EventHandler(this.lbl1o_Click);
            // 
            // lbl1x
            // 
            this.lbl1x.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl1x.Location = new System.Drawing.Point(93, 12);
            this.lbl1x.Name = "lbl1x";
            this.lbl1x.Size = new System.Drawing.Size(75, 53);
            this.lbl1x.TabIndex = 1;
            this.lbl1x.Text = "x";
            this.lbl1x.UseVisualStyleBackColor = true;
            this.lbl1x.Visible = false;
            this.lbl1x.Click += new System.EventHandler(this.lbl1x_Click);
            // 
            // lbl2o
            // 
            this.lbl2o.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl2o.Location = new System.Drawing.Point(209, 12);
            this.lbl2o.Name = "lbl2o";
            this.lbl2o.Size = new System.Drawing.Size(75, 53);
            this.lbl2o.TabIndex = 2;
            this.lbl2o.Text = "o";
            this.lbl2o.UseVisualStyleBackColor = true;
            this.lbl2o.Visible = false;
            this.lbl2o.Click += new System.EventHandler(this.lbl2o_Click);
            // 
            // lbl2x
            // 
            this.lbl2x.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl2x.Location = new System.Drawing.Point(290, 12);
            this.lbl2x.Name = "lbl2x";
            this.lbl2x.Size = new System.Drawing.Size(75, 53);
            this.lbl2x.TabIndex = 3;
            this.lbl2x.Text = "x";
            this.lbl2x.UseVisualStyleBackColor = true;
            this.lbl2x.Visible = false;
            this.lbl2x.Click += new System.EventHandler(this.lbl2x_Click);
            // 
            // lbl3o
            // 
            this.lbl3o.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl3o.Location = new System.Drawing.Point(400, 12);
            this.lbl3o.Name = "lbl3o";
            this.lbl3o.Size = new System.Drawing.Size(75, 53);
            this.lbl3o.TabIndex = 4;
            this.lbl3o.Text = "o";
            this.lbl3o.UseVisualStyleBackColor = true;
            this.lbl3o.Visible = false;
            this.lbl3o.Click += new System.EventHandler(this.lbl3o_Click);
            // 
            // lbl3x
            // 
            this.lbl3x.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl3x.Location = new System.Drawing.Point(481, 12);
            this.lbl3x.Name = "lbl3x";
            this.lbl3x.Size = new System.Drawing.Size(75, 52);
            this.lbl3x.TabIndex = 5;
            this.lbl3x.Text = "x";
            this.lbl3x.UseVisualStyleBackColor = true;
            this.lbl3x.Visible = false;
            this.lbl3x.Click += new System.EventHandler(this.lbl3x_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(87, 206);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(0, 39);
            this.label1.TabIndex = 6;
            // 
            // lbl4o
            // 
            this.lbl4o.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl4o.Location = new System.Drawing.Point(12, 71);
            this.lbl4o.Name = "lbl4o";
            this.lbl4o.Size = new System.Drawing.Size(75, 53);
            this.lbl4o.TabIndex = 7;
            this.lbl4o.Text = "o";
            this.lbl4o.UseVisualStyleBackColor = true;
            this.lbl4o.Visible = false;
            this.lbl4o.Click += new System.EventHandler(this.lbl4o_Click);
            // 
            // lbl4x
            // 
            this.lbl4x.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl4x.Location = new System.Drawing.Point(93, 71);
            this.lbl4x.Name = "lbl4x";
            this.lbl4x.Size = new System.Drawing.Size(75, 53);
            this.lbl4x.TabIndex = 8;
            this.lbl4x.Text = "x";
            this.lbl4x.UseVisualStyleBackColor = true;
            this.lbl4x.Visible = false;
            this.lbl4x.Click += new System.EventHandler(this.lbl4x_Click);
            // 
            // lbl5o
            // 
            this.lbl5o.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl5o.Location = new System.Drawing.Point(209, 71);
            this.lbl5o.Name = "lbl5o";
            this.lbl5o.Size = new System.Drawing.Size(75, 53);
            this.lbl5o.TabIndex = 9;
            this.lbl5o.Text = "o";
            this.lbl5o.UseVisualStyleBackColor = true;
            this.lbl5o.Visible = false;
            this.lbl5o.Click += new System.EventHandler(this.lbl5o_Click);
            // 
            // lbl5x
            // 
            this.lbl5x.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl5x.Location = new System.Drawing.Point(290, 71);
            this.lbl5x.Name = "lbl5x";
            this.lbl5x.Size = new System.Drawing.Size(75, 53);
            this.lbl5x.TabIndex = 10;
            this.lbl5x.Text = "x";
            this.lbl5x.UseVisualStyleBackColor = true;
            this.lbl5x.Visible = false;
            this.lbl5x.Click += new System.EventHandler(this.lbl5x_Click);
            // 
            // lbl6o
            // 
            this.lbl6o.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl6o.Location = new System.Drawing.Point(400, 71);
            this.lbl6o.Name = "lbl6o";
            this.lbl6o.Size = new System.Drawing.Size(75, 53);
            this.lbl6o.TabIndex = 11;
            this.lbl6o.Text = "o";
            this.lbl6o.UseVisualStyleBackColor = true;
            this.lbl6o.Visible = false;
            this.lbl6o.Click += new System.EventHandler(this.lbl6o_Click);
            // 
            // lbl6x
            // 
            this.lbl6x.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl6x.Location = new System.Drawing.Point(481, 70);
            this.lbl6x.Name = "lbl6x";
            this.lbl6x.Size = new System.Drawing.Size(75, 54);
            this.lbl6x.TabIndex = 12;
            this.lbl6x.Text = "x";
            this.lbl6x.UseVisualStyleBackColor = true;
            this.lbl6x.Visible = false;
            this.lbl6x.Click += new System.EventHandler(this.lbl6x_Click);
            // 
            // lbl7o
            // 
            this.lbl7o.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl7o.Location = new System.Drawing.Point(12, 130);
            this.lbl7o.Name = "lbl7o";
            this.lbl7o.Size = new System.Drawing.Size(75, 53);
            this.lbl7o.TabIndex = 13;
            this.lbl7o.Text = "o";
            this.lbl7o.UseVisualStyleBackColor = true;
            this.lbl7o.Visible = false;
            this.lbl7o.Click += new System.EventHandler(this.lbl7o_Click);
            // 
            // lbl7x
            // 
            this.lbl7x.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl7x.Location = new System.Drawing.Point(92, 130);
            this.lbl7x.Name = "lbl7x";
            this.lbl7x.Size = new System.Drawing.Size(75, 53);
            this.lbl7x.TabIndex = 14;
            this.lbl7x.Text = "x";
            this.lbl7x.UseVisualStyleBackColor = true;
            this.lbl7x.Visible = false;
            this.lbl7x.Click += new System.EventHandler(this.lbl7x_Click);
            // 
            // lbl8o
            // 
            this.lbl8o.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl8o.Location = new System.Drawing.Point(209, 130);
            this.lbl8o.Name = "lbl8o";
            this.lbl8o.Size = new System.Drawing.Size(75, 53);
            this.lbl8o.TabIndex = 15;
            this.lbl8o.Text = "o";
            this.lbl8o.UseVisualStyleBackColor = true;
            this.lbl8o.Visible = false;
            this.lbl8o.Click += new System.EventHandler(this.lbl8o_Click);
            // 
            // lbl8x
            // 
            this.lbl8x.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl8x.Location = new System.Drawing.Point(290, 130);
            this.lbl8x.Name = "lbl8x";
            this.lbl8x.Size = new System.Drawing.Size(75, 53);
            this.lbl8x.TabIndex = 16;
            this.lbl8x.Text = "x";
            this.lbl8x.UseVisualStyleBackColor = true;
            this.lbl8x.Visible = false;
            this.lbl8x.Click += new System.EventHandler(this.lbl8x_Click);
            // 
            // lbl9o
            // 
            this.lbl9o.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl9o.Location = new System.Drawing.Point(400, 130);
            this.lbl9o.Name = "lbl9o";
            this.lbl9o.Size = new System.Drawing.Size(75, 53);
            this.lbl9o.TabIndex = 17;
            this.lbl9o.Text = "o";
            this.lbl9o.UseVisualStyleBackColor = true;
            this.lbl9o.Visible = false;
            this.lbl9o.Click += new System.EventHandler(this.lbl9o_Click);
            // 
            // lbl9x
            // 
            this.lbl9x.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl9x.Location = new System.Drawing.Point(481, 130);
            this.lbl9x.Name = "lbl9x";
            this.lbl9x.Size = new System.Drawing.Size(75, 53);
            this.lbl9x.TabIndex = 18;
            this.lbl9x.Text = "x";
            this.lbl9x.UseVisualStyleBackColor = true;
            this.lbl9x.Visible = false;
            this.lbl9x.Click += new System.EventHandler(this.lbl9x_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(284, 206);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(0, 39);
            this.label2.TabIndex = 19;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(475, 206);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(0, 39);
            this.label3.TabIndex = 20;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(87, 296);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(0, 39);
            this.label4.TabIndex = 21;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(284, 295);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(0, 39);
            this.label5.TabIndex = 22;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(475, 295);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(0, 39);
            this.label6.TabIndex = 23;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(87, 374);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(0, 39);
            this.label7.TabIndex = 24;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(284, 373);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(0, 39);
            this.label8.TabIndex = 25;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(475, 373);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(0, 39);
            this.label9.TabIndex = 26;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(757, 26);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(0, 31);
            this.label10.TabIndex = 27;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(757, 85);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(0, 31);
            this.label11.TabIndex = 28;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(578, 146);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(0, 37);
            this.label12.TabIndex = 29;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(565, 85);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(170, 31);
            this.label14.TabIndex = 31;
            this.label14.Text = "Wygrane X - ";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(562, 26);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(173, 31);
            this.label15.TabIndex = 32;
            this.label15.Text = "Wygrane O - ";
            // 
            // gvg
            // 
            this.gvg.Location = new System.Drawing.Point(128, 160);
            this.gvg.Name = "gvg";
            this.gvg.Size = new System.Drawing.Size(156, 23);
            this.gvg.TabIndex = 33;
            this.gvg.Text = "gracz vs gracz";
            this.gvg.UseVisualStyleBackColor = true;
            this.gvg.Click += new System.EventHandler(this.gvg_Click);
            // 
            // gvk
            // 
            this.gvk.Location = new System.Drawing.Point(341, 160);
            this.gvk.Name = "gvk";
            this.gvk.Size = new System.Drawing.Size(159, 23);
            this.gvk.TabIndex = 34;
            this.gvk.Text = "gracz vs komputer";
            this.gvk.UseVisualStyleBackColor = true;
            this.gvk.Click += new System.EventHandler(this.gvk_Click);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(735, 428);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(41, 13);
            this.label13.TabIndex = 35;
            this.label13.Text = "label13";
            // 
            // czyszczenieWynikow
            // 
            this.czyszczenieWynikow.Location = new System.Drawing.Point(613, 332);
            this.czyszczenieWynikow.Name = "czyszczenieWynikow";
            this.czyszczenieWynikow.Size = new System.Drawing.Size(129, 23);
            this.czyszczenieWynikow.TabIndex = 36;
            this.czyszczenieWynikow.Text = "Restart punktacji";
            this.czyszczenieWynikow.UseVisualStyleBackColor = true;
            this.czyszczenieWynikow.Click += new System.EventHandler(this.czyszczenieWynikow_Click);
            // 
            // wyczyszcPlansze
            // 
            this.wyczyszcPlansze.Location = new System.Drawing.Point(613, 362);
            this.wyczyszcPlansze.Name = "wyczyszcPlansze";
            this.wyczyszcPlansze.Size = new System.Drawing.Size(129, 23);
            this.wyczyszcPlansze.TabIndex = 37;
            this.wyczyszcPlansze.Text = "Czyszczenie planszy";
            this.wyczyszcPlansze.UseVisualStyleBackColor = true;
            this.wyczyszcPlansze.Click += new System.EventHandler(this.wyczyszcPlansze_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.wyczyszcPlansze);
            this.Controls.Add(this.czyszczenieWynikow);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.gvk);
            this.Controls.Add(this.gvg);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lbl9x);
            this.Controls.Add(this.lbl9o);
            this.Controls.Add(this.lbl8x);
            this.Controls.Add(this.lbl8o);
            this.Controls.Add(this.lbl7x);
            this.Controls.Add(this.lbl7o);
            this.Controls.Add(this.lbl6x);
            this.Controls.Add(this.lbl6o);
            this.Controls.Add(this.lbl5x);
            this.Controls.Add(this.lbl5o);
            this.Controls.Add(this.lbl4x);
            this.Controls.Add(this.lbl4o);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lbl3x);
            this.Controls.Add(this.lbl3o);
            this.Controls.Add(this.lbl2x);
            this.Controls.Add(this.lbl2o);
            this.Controls.Add(this.lbl1x);
            this.Controls.Add(this.lbl1o);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button lbl1o;
        private System.Windows.Forms.Button lbl1x;
        private System.Windows.Forms.Button lbl2o;
        private System.Windows.Forms.Button lbl2x;
        private System.Windows.Forms.Button lbl3o;
        private System.Windows.Forms.Button lbl3x;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button lbl4o;
        private System.Windows.Forms.Button lbl4x;
        private System.Windows.Forms.Button lbl5o;
        private System.Windows.Forms.Button lbl5x;
        private System.Windows.Forms.Button lbl6o;
        private System.Windows.Forms.Button lbl6x;
        private System.Windows.Forms.Button lbl7o;
        private System.Windows.Forms.Button lbl7x;
        private System.Windows.Forms.Button lbl8o;
        private System.Windows.Forms.Button lbl8x;
        private System.Windows.Forms.Button lbl9o;
        private System.Windows.Forms.Button lbl9x;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Button gvg;
        private System.Windows.Forms.Button gvk;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Button czyszczenieWynikow;
        private System.Windows.Forms.Button wyczyszcPlansze;
    }
}

