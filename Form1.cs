﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    { int wygranaO = 0, wygranaX = 0, kolej = 0;
        bool zkomp = false, wstawieBoWygram = false, wygranaxzkomp = false, wygranaokomputera = false;
        public Form1()
        {
            InitializeComponent();
        }

        private void kolejka()
        { if (zkomp == false)
            {
                if (kolej % 2 == 0)
                {
                    lbl1x.Visible = false;
                    lbl2x.Visible = false;
                    lbl3x.Visible = false;
                    lbl4x.Visible = false;
                    lbl5x.Visible = false;
                    lbl6x.Visible = false;
                    lbl7x.Visible = false;
                    lbl8x.Visible = false;
                    lbl9x.Visible = false;

                    lbl1o.Visible = true;
                    lbl2o.Visible = true;
                    lbl3o.Visible = true;
                    lbl4o.Visible = true;
                    lbl5o.Visible = true;
                    lbl6o.Visible = true;
                    lbl7o.Visible = true;
                    lbl8o.Visible = true;
                    lbl9o.Visible = true;

                    kolej++;
                }
                else
                {
                    lbl1o.Visible = false;
                    lbl2o.Visible = false;
                    lbl3o.Visible = false;
                    lbl4o.Visible = false;
                    lbl5o.Visible = false;
                    lbl6o.Visible = false;
                    lbl7o.Visible = false;
                    lbl8o.Visible = false;
                    lbl9o.Visible = false;

                    lbl2x.Visible = true;
                    lbl1x.Visible = true;
                    lbl3x.Visible = true;
                    lbl4x.Visible = true;
                    lbl5x.Visible = true;
                    lbl6x.Visible = true;
                    lbl7x.Visible = true;
                    lbl8x.Visible = true;
                    lbl9x.Visible = true;
                    kolej++;
                }
            }

        }

        private void lbl1o_Click(object sender, EventArgs e)
        {
            kolejka();
            label1.Text = "o";
            lbl1o.Enabled = false;
            lbl1x.Enabled = false;
            czyWygrana();
        }

        private void lbl1x_Click(object sender, EventArgs e)
        {
            kolejka();
            label1.Text = "x";
            lbl1o.Enabled = false;
            lbl1x.Enabled = false;
            czyWygrana();
            if (zkomp == true)
            {
                drugiRuchKomp();
            }
        }

        private void lbl2o_Click(object sender, EventArgs e)
        {
            kolejka();
            label2.Text = "o";
            lbl2o.Enabled = false;
            lbl2x.Enabled = false;
            czyWygrana();
            if (zkomp == true)
            {
                drugiRuchKomp();
            }
        }

        private void lbl2x_Click(object sender, EventArgs e)
        {
            kolejka();
            label2.Text = "x";
            lbl2o.Enabled = false;
            lbl2x.Enabled = false;
            czyWygrana();
            if (zkomp == true)
            {
                drugiRuchKomp();
            }
        }

        private void lbl3o_Click(object sender, EventArgs e)
        {
            kolejka();
            label3.Text = "o";
            lbl3o.Enabled = false;
            lbl3x.Enabled = false;
            czyWygrana();
        }

        private void lbl3x_Click(object sender, EventArgs e)
        {
            kolejka();
            label3.Text = "x";
            lbl3o.Enabled = false;
            lbl3x.Enabled = false;
            czyWygrana();
            if (zkomp == true)
            {
                drugiRuchKomp();
            }

        }

        private void lbl4o_Click(object sender, EventArgs e)
        {
            kolejka();
            label4.Text = "o";
            lbl4o.Enabled = false;
            lbl4x.Enabled = false;
            czyWygrana();
        }

        private void lbl4x_Click(object sender, EventArgs e)
        {
            kolejka();
            label4.Text = "x";
            lbl4o.Enabled = false;
            lbl4x.Enabled = false;
            czyWygrana();
            if (zkomp == true)
            {
                drugiRuchKomp();
            }
        }

        private void lbl5o_Click(object sender, EventArgs e)
        {
            kolejka();
            label5.Text = "o";
            lbl5o.Enabled = false;
            lbl5x.Enabled = false;
            czyWygrana();

        }

        private void lbl5x_Click(object sender, EventArgs e)
        {
            kolejka();
            label5.Text = "x";
            lbl5o.Enabled = false;
            lbl5x.Enabled = false;
            czyWygrana();
            if (zkomp == true)
            {
                drugiRuchKomp();
            }
        }

        private void lbl6o_Click(object sender, EventArgs e)
        {
            kolejka();
            label6.Text = "o";
            lbl6o.Enabled = false;
            lbl6x.Enabled = false;
            czyWygrana();
        }

        private void lbl6x_Click(object sender, EventArgs e)
        {
            kolejka();
            label6.Text = "x";
            lbl6o.Enabled = false;
            lbl6x.Enabled = false;
            czyWygrana();
            if (zkomp == true)
            {
                drugiRuchKomp();
            }
        }

        private void lbl7o_Click(object sender, EventArgs e)
        {
            kolejka();
            label7.Text = "o";
            lbl7o.Enabled = false;
            lbl7x.Enabled = false;
            czyWygrana();
            if (zkomp == true)
            {
                drugiRuchKomp();
            }
        }

        private void lbl7x_Click(object sender, EventArgs e)
        {
            kolejka();
            label7.Text = "x";
            lbl7o.Enabled = false;
            lbl7x.Enabled = false;
            czyWygrana();
            if (zkomp == true)
            {
                drugiRuchKomp();
            }
        }

        private void lbl8o_Click(object sender, EventArgs e)
        {
            kolejka();
            label8.Text = "o";
            lbl8o.Enabled = false;
            lbl8x.Enabled = false;
            czyWygrana();

        }

        private void lbl8x_Click(object sender, EventArgs e)
        {
            kolejka();
            label8.Text = "x";
            lbl8o.Enabled = false;
            lbl8x.Enabled = false;
            czyWygrana();
            if (zkomp == true)
            {
                drugiRuchKomp();
            }
        }

        private void lbl9o_Click(object sender, EventArgs e)
        {
            kolejka();
            label9.Text = "o";
            lbl9o.Enabled = false;
            lbl9x.Enabled = false;
            czyWygrana();
        }

        private void lbl9x_Click(object sender, EventArgs e)
        {
            kolejka();
            label9.Text = "x";
            lbl9o.Enabled = false;
            lbl9x.Enabled = false;
            czyWygrana();
            if (zkomp == true)
            {
                drugiRuchKomp();
            }
        }

        private void czyWygrana()
        {

            if ((label1.Text == "o" && label2.Text == "o" && label3.Text == "o") || (label1.Text == "x" && label2.Text == "x" && label3.Text == "x"))
            {
                if (label1.Text == "o")
                {
                    wygranaO++;
                    label10.Text = Convert.ToString(wygranaO);
                    label12.Text = "Wygral O";
                    wygranaokomputera = true;
                    wylaczeniePrzyciskow();
                    //czyszczeniePlanszy();
                }
                else
                {
                    wygranaX++;
                    label11.Text = Convert.ToString(wygranaX);
                    label12.Text = "Wygral X";
                    wygranaxzkomp = true;
                    wylaczeniePrzyciskow();
                    //czyszczeniePlanszy();
                }
            }//1 if
            else
            {
                if ((label1.Text == "o" && label5.Text == "o" && label9.Text == "o") || (label1.Text == "x" && label5.Text == "x" && label9.Text == "x"))
                {
                    if (label1.Text == "o")
                    {
                        wygranaO++;
                        label10.Text = Convert.ToString(wygranaO);
                        label12.Text = "Wygral O";
                        wygranaokomputera = true;
                        wylaczeniePrzyciskow();
                        //czyszczeniePlanszy();
                    }
                    else
                    {
                        wygranaX++;
                        label11.Text = Convert.ToString(wygranaX);
                        label12.Text = "Wygral X";
                        wygranaxzkomp = true;
                        wylaczeniePrzyciskow();
                        //czyszczeniePlanszy();
                    }
                }//2 if
                else
                {
                    if ((label1.Text == "o" && label4.Text == "o" && label7.Text == "o") || (label1.Text == "x" && label4.Text == "x" && label7.Text == "x"))
                    {
                        if (label1.Text == "o")
                        {
                            wygranaO++;
                            label10.Text = Convert.ToString(wygranaO);
                            label12.Text = "Wygral O";
                            wygranaokomputera = true;
                            wylaczeniePrzyciskow();
                            //czyszczeniePlanszy();
                        }
                        else
                        {
                            wygranaX++;
                            label11.Text = Convert.ToString(wygranaX);
                            label12.Text = "Wygral X";
                            wygranaxzkomp = true;
                            wylaczeniePrzyciskow();
                            //czyszczeniePlanszy();
                        }
                    }//3 if
                    else
                    {
                        if ((label2.Text == "o" && label5.Text == "o" && label8.Text == "o") || (label2.Text == "x" && label5.Text == "x" && label8.Text == "x"))
                        {
                            if (label2.Text == "o")
                            {
                                wygranaO++;
                                label10.Text = Convert.ToString(wygranaO);
                                label12.Text = "Wygral O";
                                wygranaokomputera = true;
                                wylaczeniePrzyciskow();
                                // czyszczeniePlanszy();
                            }
                            else
                            {
                                wygranaX++;
                                label11.Text = Convert.ToString(wygranaX);
                                label12.Text = "Wygral X";
                                wygranaxzkomp = true;
                                wylaczeniePrzyciskow();
                                //czyszczeniePlanszy();
                            }
                        }//4if
                        else
                        {
                            if ((label3.Text == "o" && label6.Text == "o" && label9.Text == "o") || (label3.Text == "x" && label6.Text == "x" && label9.Text == "x"))
                            {
                                if (label3.Text == "o")
                                {
                                    wygranaO++;
                                    label10.Text = Convert.ToString(wygranaO);
                                    label12.Text = "Wygral O";
                                    wygranaokomputera = true;
                                    wylaczeniePrzyciskow();
                                    //czyszczeniePlanszy();
                                }
                                else
                                {
                                    wygranaX++;
                                    label11.Text = Convert.ToString(wygranaX);
                                    label12.Text = "Wygral X";
                                    wygranaxzkomp = true;
                                    wylaczeniePrzyciskow();
                                    //czyszczeniePlanszy();
                                }
                            }//5 if
                            else
                            {
                                if ((label4.Text == "o" && label5.Text == "o" && label6.Text == "o") || (label4.Text == "x" && label5.Text == "x" && label6.Text == "x"))
                                {
                                    if (label4.Text == "o")
                                    {
                                        wygranaO++;
                                        label10.Text = Convert.ToString(wygranaO);
                                        label12.Text = "Wygral O";
                                        wygranaokomputera = true;
                                        wylaczeniePrzyciskow();
                                        //czyszczeniePlanszy();
                                    }
                                    else
                                    {
                                        wygranaX++;
                                        label11.Text = Convert.ToString(wygranaX);
                                        label12.Text = "Wygral X";
                                        wygranaxzkomp = true;
                                        wylaczeniePrzyciskow();
                                        //czyszczeniePlanszy();
                                    }
                                }//6if
                                else
                                {
                                    if ((label7.Text == "o" && label8.Text == "o" && label9.Text == "o") || (label7.Text == "x" && label8.Text == "x" && label9.Text == "x"))
                                    {
                                        if (label7.Text == "o")
                                        {
                                            wygranaO++;
                                            label10.Text = Convert.ToString(wygranaO);
                                            label12.Text = "Wygral O";
                                            wygranaokomputera = true;
                                            wylaczeniePrzyciskow();
                                            // czyszczeniePlanszy();
                                        }
                                        else
                                        {
                                            wygranaX++;
                                            label11.Text = Convert.ToString(wygranaX);
                                            label12.Text = "Wygral X";
                                            wygranaxzkomp = true;
                                            wylaczeniePrzyciskow();
                                            // czyszczeniePlanszy();
                                        }
                                    }//7 if
                                    else
                                    {
                                        if ((label3.Text == "o" && label5.Text == "o" && label7.Text == "o") || (label3.Text == "x" && label5.Text == "x" && label7.Text == "x"))
                                        {
                                            if (label3.Text == "o")
                                            {
                                                wygranaO++;
                                                label10.Text = Convert.ToString(wygranaO);
                                                label12.Text = "Wygral O";
                                                wygranaokomputera = true;
                                                wylaczeniePrzyciskow();
                                                //czyszczeniePlanszy();
                                            }
                                            else
                                            {
                                                wygranaX++;
                                                label11.Text = Convert.ToString(wygranaX);
                                                label12.Text = "Wygral X";
                                                wygranaxzkomp = true;
                                                wylaczeniePrzyciskow();
                                                //czyszczeniePlanszy();
                                            }
                                        }//8if
                                        else czyPuste();
                                        //8 else

                                    }//7 else
                                }//6 else
                            }//5 else
                        }//4 else
                    }//3 else
                }//2 else
            }//1 else
        }

        private void czyszczeniePlanszy()
        {
            label1.Text = String.Empty;
            label2.Text = String.Empty;
            label3.Text = String.Empty;
            label4.Text = String.Empty;
            label5.Text = String.Empty;
            label6.Text = String.Empty;
            label7.Text = String.Empty;
            label8.Text = String.Empty;
            label9.Text = String.Empty;

            lbl1o.Enabled = true;
            lbl1x.Enabled = true;
            lbl2o.Enabled = true;
            lbl2x.Enabled = true;
            lbl3o.Enabled = true;
            lbl3x.Enabled = true;
            lbl4o.Enabled = true;
            lbl4x.Enabled = true;
            lbl5o.Enabled = true;
            lbl5x.Enabled = true;
            lbl6o.Enabled = true;
            lbl6x.Enabled = true;
            lbl7o.Enabled = true;
            lbl7x.Enabled = true;
            lbl8o.Enabled = true;
            lbl8x.Enabled = true;
            lbl9o.Enabled = true;
            lbl9x.Enabled = true;

            lbl1o.Visible = false;
            lbl2o.Visible = false;
            lbl3o.Visible = false;
            lbl4o.Visible = false;
            lbl5o.Visible = false;
            lbl6o.Visible = false;
            lbl7o.Visible = false;
            lbl8o.Visible = false;
            lbl9o.Visible = false;

            lbl1x.Visible = false;
            lbl2x.Visible = false;
            lbl3x.Visible = false;
            lbl4x.Visible = false;
            lbl5x.Visible = false;
            lbl6x.Visible = false;
            lbl7x.Visible = false;
            lbl8x.Visible = false;
            lbl9x.Visible = false;
            gvg.Visible = true;
            gvk.Visible = true;
            zkomp = false;
            wygranaokomputera = false;
            wygranaxzkomp = false;
        }

        private void gvg_Click(object sender, EventArgs e)
        {
            gvg.Visible = false;
            gvk.Visible = false;
            kolejka();
        }

        private void wylaczeniePrzyciskow()
        {
            lbl1o.Enabled = false;
            lbl1x.Enabled = false;
            lbl2o.Enabled = false;
            lbl2x.Enabled = false;
            lbl3o.Enabled = false;
            lbl3x.Enabled = false;
            lbl4o.Enabled = false;
            lbl4x.Enabled = false;
            lbl5o.Enabled = false;
            lbl5x.Enabled = false;
            lbl6o.Enabled = false;
            lbl6x.Enabled = false;
            lbl7o.Enabled = false;
            lbl7x.Enabled = false;
            lbl8o.Enabled = false;
            lbl8x.Enabled = false;
            lbl9o.Enabled = false;
            lbl9x.Enabled = false;
        }

        private void gvk_Click(object sender, EventArgs e)
        {
            zkomp = true;
            gvk.Visible = false;
            gvg.Visible = false;
            lbl1o.Visible = false;
            lbl2o.Visible = false;
            lbl3o.Visible = false;
            lbl4o.Visible = false;
            lbl5o.Visible = false;
            lbl6o.Visible = false;
            lbl7o.Visible = false;
            lbl8o.Visible = false;
            lbl9o.Visible = false;

            lbl1x.Visible = false;
            lbl2x.Visible = false;
            lbl3x.Visible = false;
            lbl4x.Visible = false;
            lbl5x.Visible = false;
            lbl6x.Visible = false;
            lbl7x.Visible = false;
            lbl8x.Visible = false;
            lbl9x.Visible = false;
            ruchKomputera();

        }

        private void wlaczenieX()
        {
            lbl1x.Visible = true;
            lbl2x.Visible = true;
            lbl3x.Visible = true;
            lbl4x.Visible = true;
            lbl5x.Visible = true;
            lbl6x.Visible = true;
            lbl7x.Visible = true;
            lbl8x.Visible = true;
            lbl9x.Visible = true;
        }

        private void ruchKomputera()
        {
            Random pole1 = new Random();
            int p = 1, k = 10;
            int wpole1 = pole1.Next(p, k);
            label13.Text = Convert.ToString(wpole1);
            if (wpole1 == 1)
            {
                label1.Text = "o";
                lbl1x.Enabled = false;
                wlaczenieX();
            }
            else
            {
                if (wpole1 == 2)
                {
                    label2.Text = "o";
                    lbl2x.Enabled = false;
                    wlaczenieX();
                }
                else
                {
                    if (wpole1 == 3)
                    {
                        label3.Text = "o";
                        lbl3x.Enabled = false;
                        wlaczenieX();
                    }
                    else
                    {
                        if (wpole1 == 4)
                        {
                            label4.Text = "o";
                            lbl4x.Enabled = false;
                            wlaczenieX();
                        }
                        else
                        {
                            if (wpole1 == 5)
                            {
                                label5.Text = "o";
                                lbl5x.Enabled = false;
                                wlaczenieX();
                            }
                            else
                            {
                                if (wpole1 == 6)
                                {
                                    label6.Text = "o";
                                    lbl6x.Enabled = false;
                                    wlaczenieX();
                                }
                                else
                                {
                                    if (wpole1 == 7)
                                    { label7.Text = "o";
                                        lbl7x.Enabled = false;
                                        wlaczenieX();
                                    }
                                    else
                                    {
                                        if (wpole1 == 8)
                                        { label8.Text = "o";
                                            lbl8x.Enabled = false;
                                            wlaczenieX();
                                        }
                                        else
                                        {
                                            if (wpole1 == 9)
                                            { label9.Text = "o";
                                                lbl9x.Enabled = false;
                                                wlaczenieX();
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        private void drugiRuchKomp()
        {
            Random pole2 = new Random();
            int wpole2;

            if (lbl1x.Enabled == false && (label1.Text == "x" || label1.Text == "o") && wygranaxzkomp==false)
            {
            wybor:
                { wpole2 = pole2.Next(1, 9); czyWygramJakWstawie(); }

                label13.Text = Convert.ToString(wpole2);
                if (wstawieBoWygram == false)
                {
                    if (wpole2 == 1 && label5.Text == "")
                    {
                        label5.Text = "o";
                        lbl5x.Enabled = false;
                        czyWygrana();
                    }
                    else
                    {
                        if (wpole2 == 2 && label2.Text == "")
                        {
                            label2.Text = "o";
                            lbl2x.Enabled = false;
                            czyWygrana();
                        }
                        else
                        {
                            if (wpole2 == 3 && label3.Text == "")
                            {
                                label3.Text = "o";
                                lbl3x.Enabled = false;
                                czyWygrana();
                            }
                            else
                            {
                                if (wpole2 == 4 && label4.Text == "")
                                {
                                    label4.Text = "o";
                                    lbl4x.Enabled = false;
                                    czyWygrana();
                                }
                                else
                                {
                                    if (wpole2 == 5 && label6.Text == "")
                                    {
                                        label6.Text = "o";
                                        lbl6x.Enabled = false;
                                        czyWygrana();
                                    }
                                    else
                                    {
                                        if (wpole2 == 6 && label7.Text == "")
                                        {
                                            label7.Text = "o";
                                            lbl7x.Enabled = false;
                                            czyWygrana();
                                        }
                                        else
                                        {
                                            if (wpole2 == 7 && label8.Text == "")
                                            {
                                                label8.Text = "o";
                                                lbl8x.Enabled = false;
                                                czyWygrana();
                                            }
                                            else
                                            {
                                                if (label9.Text == "")
                                                {
                                                    label9.Text = "o";
                                                    lbl9x.Enabled = false;
                                                    czyWygrana();
                                                }
                                                else goto wybor;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            else//1
            {
                if (lbl3x.Enabled == false && (label3.Text == "x" || label3.Text == "o") && wygranaxzkomp == false)
                {
                wybor:
                    { wpole2 = pole2.Next(1, 9); czyWygramJakWstawie(); }

                    label13.Text = Convert.ToString(wpole2);
                    if (wstawieBoWygram == false)
                    {
                        if (wpole2 == 1 && label1.Text == "")
                        {
                            label1.Text = "o";
                            lbl1x.Enabled = false;
                            czyWygrana();
                        }
                        else
                        {
                            if (wpole2 == 2 && label2.Text == "")
                            {
                                label2.Text = "o";
                                lbl2x.Enabled = false;
                                czyWygrana();
                            }
                            else
                            {
                                if (wpole2 == 3 && label5.Text == "")
                                {
                                    label5.Text = "o";
                                    lbl5x.Enabled = false;
                                    czyWygrana();
                                }
                                else
                                {
                                    if (wpole2 == 4 && label4.Text == "")
                                    {
                                        label4.Text = "o";
                                        lbl4x.Enabled = false;
                                        czyWygrana();
                                    }
                                    else
                                    {
                                        if (wpole2 == 5 && label6.Text == "")
                                        {
                                            label6.Text = "o";
                                            lbl6x.Enabled = false;
                                            czyWygrana();
                                        }
                                        else
                                        {
                                            if (wpole2 == 6 && label7.Text == "")
                                            {
                                                label7.Text = "o";
                                                lbl7x.Enabled = false;
                                                czyWygrana();
                                            }
                                            else
                                            {
                                                if (wpole2 == 7 && label8.Text == "")
                                                {
                                                    label8.Text = "o";
                                                    lbl8x.Enabled = false;
                                                    czyWygrana();
                                                }
                                                else
                                                {
                                                    if (label9.Text == "")
                                                    {
                                                        label9.Text = "o";
                                                        lbl9x.Enabled = false;
                                                        czyWygrana();
                                                    }
                                                    else goto wybor;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                else//2
                {
                    if (lbl7x.Enabled == false && (label7.Text == "x" || label7.Text == "o") && wygranaxzkomp == false)
                    {
                    wybor:
                        { wpole2 = pole2.Next(1, 9); czyWygramJakWstawie(); }

                        label13.Text = Convert.ToString(wpole2);
                        if (wstawieBoWygram == false)
                        {
                            if (wpole2 == 1 && label1.Text == "")
                            {
                                label1.Text = "o";
                                lbl1x.Enabled = false;
                                czyWygrana();
                            }
                            else
                            {
                                if (wpole2 == 2 && label2.Text == "")
                                {
                                    label2.Text = "o";
                                    lbl2x.Enabled = false;
                                    czyWygrana();
                                }
                                else
                                {
                                    if (wpole2 == 3 && label3.Text == "")
                                    {
                                        label3.Text = "o";
                                        lbl3x.Enabled = false;
                                        czyWygrana();
                                    }
                                    else
                                    {
                                        if (wpole2 == 4 && label4.Text == "")
                                        {
                                            label4.Text = "o";
                                            lbl4x.Enabled = false;
                                            czyWygrana();
                                        }
                                        else
                                        {
                                            if (wpole2 == 5 && label6.Text == "")
                                            {
                                                label6.Text = "o";
                                                lbl6x.Enabled = false;
                                                czyWygrana();
                                            }
                                            else
                                            {
                                                if (wpole2 == 6 && label5.Text == "")
                                                {
                                                    label5.Text = "o";
                                                    lbl5x.Enabled = false;
                                                    czyWygrana();
                                                }
                                                else
                                                {
                                                    if (wpole2 == 7 && label8.Text == "")
                                                    {
                                                        label8.Text = "o";
                                                        lbl8x.Enabled = false;
                                                        czyWygrana();
                                                    }
                                                    else
                                                    {
                                                        if (label9.Text == "")
                                                        {
                                                            label9.Text = "o";
                                                            lbl9x.Enabled = false;
                                                            czyWygrana();
                                                        }
                                                        else goto wybor;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else//3


                    {
                        if (lbl9x.Enabled == false && (label9.Text == "x" || label9.Text == "o") && wygranaxzkomp == false)
                        {
                        wybor:
                            { wpole2 = pole2.Next(1, 9); czyWygramJakWstawie(); }

                            label13.Text = Convert.ToString(wpole2);
                            if (wstawieBoWygram == false)
                            {
                                if (wpole2 == 1 && label1.Text == "")
                                {
                                    label1.Text = "o";
                                    lbl1x.Enabled = false;
                                    czyWygrana();
                                }
                                else
                                {
                                    if (wpole2 == 2 && label2.Text == "")
                                    {
                                        label2.Text = "o";
                                        lbl2x.Enabled = false;
                                        czyWygrana();
                                    }
                                    else
                                    {
                                        if (wpole2 == 3 && label3.Text == "")
                                        {
                                            label3.Text = "o";
                                            lbl3x.Enabled = false;
                                            czyWygrana();
                                        }
                                        else
                                        {
                                            if (wpole2 == 4 && label4.Text == "")
                                            {
                                                label4.Text = "o";
                                                lbl4x.Enabled = false;
                                                czyWygrana();
                                            }
                                            else
                                            {
                                                if (wpole2 == 5 && label6.Text == "")
                                                {
                                                    label6.Text = "o";
                                                    lbl6x.Enabled = false;
                                                    czyWygrana();
                                                }
                                                else
                                                {
                                                    if (wpole2 == 6 && label7.Text == "")
                                                    {
                                                        label7.Text = "o";
                                                        lbl7x.Enabled = false;
                                                        czyWygrana();
                                                    }
                                                    else
                                                    {
                                                        if (wpole2 == 7 && label8.Text == "")
                                                        {
                                                            label8.Text = "o";
                                                            lbl8x.Enabled = false;
                                                            czyWygrana();
                                                        }
                                                        else
                                                        {
                                                            if (label5.Text == "")
                                                            {
                                                                label5.Text = "o";
                                                                lbl9x.Enabled = false;
                                                                czyWygrana();
                                                            }
                                                            else goto wybor;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        else//4
                        {

                            if (lbl2x.Enabled == false && (label2.Text == "x" || label2.Text == "o") && wygranaxzkomp == false)
                            {
                            wybor:
                                { wpole2 = pole2.Next(1, 9); czyWygramJakWstawie(); }

                                label13.Text = Convert.ToString(wpole2);
                                if (wstawieBoWygram == false)
                                {
                                    if (wpole2 == 1 && label1.Text == "")
                                    {
                                        label1.Text = "o";
                                        lbl1x.Enabled = false;
                                        czyWygrana();
                                    }
                                    else
                                    {
                                        if (wpole2 == 2 && label5.Text == "")
                                        {
                                            label5.Text = "o";
                                            lbl5x.Enabled = false;
                                            czyWygrana();
                                        }
                                        else
                                        {
                                            if (wpole2 == 3 && label3.Text == "")
                                            {
                                                label3.Text = "o";
                                                lbl3x.Enabled = false;
                                                czyWygrana();
                                            }
                                            else
                                            {
                                                if (wpole2 == 4 && label4.Text == "")
                                                {
                                                    label4.Text = "o";
                                                    lbl4x.Enabled = false;
                                                    czyWygrana();
                                                }
                                                else
                                                {
                                                    if (wpole2 == 5 && label6.Text == "")
                                                    {
                                                        label6.Text = "o";
                                                        lbl6x.Enabled = false;
                                                        czyWygrana();
                                                    }
                                                    else
                                                    {
                                                        if (wpole2 == 6 && label7.Text == "")
                                                        {
                                                            label7.Text = "o";
                                                            lbl7x.Enabled = false;
                                                            czyWygrana();
                                                        }
                                                        else
                                                        {
                                                            if (wpole2 == 7 && label8.Text == "")
                                                            {
                                                                label8.Text = "o";
                                                                lbl8x.Enabled = false;
                                                                czyWygrana();
                                                            }
                                                            else
                                                            {
                                                                if (label9.Text == "")
                                                                {
                                                                    label9.Text = "o";
                                                                    lbl9x.Enabled = false;
                                                                    czyWygrana();
                                                                }
                                                                else goto wybor;
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            else//5
                            {
                                if (lbl6x.Enabled == false && (label6.Text == "x" || label6.Text == "o") && wygranaxzkomp == false)
                                {
                                wybor:
                                    { wpole2 = pole2.Next(1, 9); czyWygramJakWstawie(); }

                                    label13.Text = Convert.ToString(wpole2);
                                    if (wstawieBoWygram == false)
                                    {
                                        if (wpole2 == 1 && label1.Text == "")
                                        {
                                            label1.Text = "o";
                                            lbl1x.Enabled = false;
                                            czyWygrana();
                                        }
                                        else
                                        {
                                            if (wpole2 == 2 && label2.Text == "")
                                            {
                                                label2.Text = "o";
                                                lbl2x.Enabled = false;
                                                czyWygrana();
                                            }
                                            else
                                            {
                                                if (wpole2 == 3 && label3.Text == "")
                                                {
                                                    label3.Text = "o";
                                                    lbl3x.Enabled = false;
                                                    czyWygrana();
                                                }
                                                else
                                                {
                                                    if (wpole2 == 4 && label4.Text == "")
                                                    {
                                                        label4.Text = "o";
                                                        lbl4x.Enabled = false;
                                                        czyWygrana();
                                                    }
                                                    else
                                                    {
                                                        if (wpole2 == 5 && label5.Text == "")
                                                        {
                                                            label5.Text = "o";
                                                            lbl6x.Enabled = false;
                                                            czyWygrana();
                                                        }
                                                        else
                                                        {
                                                            if (wpole2 == 6 && label7.Text == "")
                                                            {
                                                                label7.Text = "o";
                                                                lbl7x.Enabled = false;
                                                                czyWygrana();
                                                            }
                                                            else
                                                            {
                                                                if (wpole2 == 7 && label8.Text == "")
                                                                {
                                                                    label8.Text = "o";
                                                                    lbl8x.Enabled = false;
                                                                    czyWygrana();
                                                                }
                                                                else
                                                                {
                                                                    if (label9.Text == "")
                                                                    {
                                                                        label9.Text = "o";
                                                                        lbl9x.Enabled = false;
                                                                        czyWygrana();
                                                                    }
                                                                    else goto wybor;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                else//6
                                {
                                    if (lbl8x.Enabled == false && (label8.Text == "x" || label8.Text == "o") && wygranaxzkomp == false)
                                    {
                                    wybor:
                                        { wpole2 = pole2.Next(1, 9); czyWygramJakWstawie(); }
                                        label13.Text = Convert.ToString(wpole2);
                                        if (wstawieBoWygram == false)
                                        {
                                            if (wpole2 == 1 && label1.Text == "")
                                            {
                                                label1.Text = "o";
                                                lbl1x.Enabled = false;

                                                czyWygrana();
                                            }
                                            else
                                            {
                                                if (wpole2 == 2 && label2.Text == "")
                                                {
                                                    label2.Text = "o";
                                                    lbl2x.Enabled = false;
                                                    czyWygrana();
                                                }
                                                else
                                                {
                                                    if (wpole2 == 3 && label3.Text == "")
                                                    {
                                                        label3.Text = "o";
                                                        lbl3x.Enabled = false;
                                                        czyWygrana();
                                                    }
                                                    else
                                                    {
                                                        if (wpole2 == 4 && label4.Text == "")
                                                        {
                                                            label4.Text = "o";
                                                            lbl4x.Enabled = false;
                                                            czyWygrana();
                                                        }
                                                        else
                                                        {
                                                            if (wpole2 == 5 && label6.Text == "")
                                                            {
                                                                label6.Text = "o";
                                                                lbl6x.Enabled = false;
                                                                czyWygrana();
                                                            }
                                                            else
                                                            {
                                                                if (wpole2 == 6 && label7.Text == "")
                                                                {
                                                                    label7.Text = "o";
                                                                    lbl7x.Enabled = false;
                                                                    czyWygrana();
                                                                }
                                                                else
                                                                {
                                                                    if (wpole2 == 7 && label5.Text == "")
                                                                    {
                                                                        label5.Text = "o";
                                                                        lbl5x.Enabled = false;
                                                                        czyWygrana();
                                                                    }
                                                                    else
                                                                    {
                                                                        if (label9.Text == "")
                                                                        {
                                                                            label9.Text = "o";
                                                                            lbl9x.Enabled = false;
                                                                            czyWygrana();
                                                                        }
                                                                        else goto wybor;
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    else//7
                                    {
                                        if (lbl4x.Enabled == false && (label4.Text == "x" || label4.Text == "o") && wygranaxzkomp == false)
                                        {
                                        wybor:
                                            { wpole2 = pole2.Next(1, 9); czyWygramJakWstawie(); }

                                            label13.Text = Convert.ToString(wpole2);
                                            if (wstawieBoWygram == false)
                                            {
                                                if (wpole2 == 1 && label1.Text == "")
                                                {
                                                    label1.Text = "o";
                                                    lbl1x.Enabled = false;
                                                    czyWygrana();
                                                }
                                                else
                                                {
                                                    if (wpole2 == 2 && label2.Text == "")
                                                    {
                                                        label2.Text = "o";
                                                        lbl2x.Enabled = false;
                                                        czyWygrana();
                                                    }
                                                    else
                                                    {
                                                        if (wpole2 == 3 && label3.Text == "")
                                                        {
                                                            label3.Text = "o";
                                                            lbl3x.Enabled = false;
                                                            czyWygrana();
                                                        }
                                                        else
                                                        {
                                                            if (wpole2 == 4 && label5.Text == "")
                                                            {
                                                                label5.Text = "o";
                                                                lbl5x.Enabled = false;
                                                                czyWygrana();
                                                            }
                                                            else
                                                            {
                                                                if (wpole2 == 5 && label6.Text == "")
                                                                {
                                                                    label6.Text = "o";
                                                                    lbl6x.Enabled = false;
                                                                    czyWygrana();
                                                                }
                                                                else
                                                                {
                                                                    if (wpole2 == 6 && label7.Text == "")
                                                                    {
                                                                        label7.Text = "o";
                                                                        lbl7x.Enabled = false;
                                                                        czyWygrana();
                                                                    }
                                                                    else
                                                                    {
                                                                        if (wpole2 == 7 && label8.Text == "")
                                                                        {
                                                                            label8.Text = "o";
                                                                            lbl8x.Enabled = false;
                                                                            czyWygrana();
                                                                        }
                                                                        else
                                                                        {
                                                                            if (label9.Text == "")
                                                                            {
                                                                                label9.Text = "o";
                                                                                lbl9x.Enabled = false;
                                                                                czyWygrana();
                                                                            }
                                                                            else goto wybor;
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        else//8
                                        {
                                            if (lbl5x.Enabled == false && (label5.Text == "x" || label5.Text == "o") && wygranaxzkomp == false)
                                            {
                                            wybor:
                                                { wpole2 = pole2.Next(1, 9); czyWygramJakWstawie(); }

                                                label13.Text = Convert.ToString(wpole2);
                                                if (wstawieBoWygram == false)
                                                {
                                                    if (wpole2 == 1 && label1.Text == "")
                                                    {
                                                        label1.Text = "o";
                                                        lbl1x.Enabled = false;
                                                        czyWygrana();

                                                    }
                                                    else
                                                    {
                                                        if (wpole2 == 2 && label2.Text == "")
                                                        {
                                                            label2.Text = "o";
                                                            lbl2x.Enabled = false;
                                                            czyWygrana();
                                                        }
                                                        else
                                                        {
                                                            if (wpole2 == 3 && label3.Text == "")
                                                            {
                                                                label3.Text = "o";
                                                                lbl3x.Enabled = false;
                                                                czyWygrana();
                                                            }
                                                            else
                                                            {
                                                                if (wpole2 == 4 && label4.Text == "")
                                                                {
                                                                    label4.Text = "o";
                                                                    lbl4x.Enabled = false;
                                                                    czyWygrana();
                                                                }
                                                                else
                                                                {
                                                                    if (wpole2 == 5 && label6.Text == "")
                                                                    {
                                                                        label6.Text = "o";
                                                                        lbl6x.Enabled = false;
                                                                        czyWygrana();
                                                                    }
                                                                    else
                                                                    {
                                                                        if (wpole2 == 6 && label7.Text == "")
                                                                        {
                                                                            label7.Text = "o";
                                                                            lbl7x.Enabled = false;
                                                                            czyWygrana();
                                                                        }
                                                                        else
                                                                        {
                                                                            if (wpole2 == 7 && label8.Text == "")
                                                                            {
                                                                                label8.Text = "o";
                                                                                lbl8x.Enabled = false;
                                                                                czyWygrana();
                                                                            }
                                                                            else
                                                                            {
                                                                                if (label9.Text == "")
                                                                                {
                                                                                    label9.Text = "o";
                                                                                    lbl9x.Enabled = false;
                                                                                    czyWygrana();
                                                                                }
                                                                                else goto wybor;
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        private void czyszczenieWynikow_Click(object sender, EventArgs e)
        {
            label11.Text = String.Empty;
            label10.Text = String.Empty;
            label12.Text = String.Empty;
            wygranaO = 0;
            wygranaX = 0;
        }

        private void wyczyszcPlansze_Click(object sender, EventArgs e)
        {
            label1.Text = String.Empty;
            label2.Text = String.Empty;
            label3.Text = String.Empty;
            label4.Text = String.Empty;
            label5.Text = String.Empty;
            label6.Text = String.Empty;
            label7.Text = String.Empty;
            label8.Text = String.Empty;
            label9.Text = String.Empty;

            lbl1o.Enabled = true;
            lbl1x.Enabled = true;
            lbl2o.Enabled = true;
            lbl2x.Enabled = true;
            lbl3o.Enabled = true;
            lbl3x.Enabled = true;
            lbl4o.Enabled = true;
            lbl4x.Enabled = true;
            lbl5o.Enabled = true;
            lbl5x.Enabled = true;
            lbl6o.Enabled = true;
            lbl6x.Enabled = true;
            lbl7o.Enabled = true;
            lbl7x.Enabled = true;
            lbl8o.Enabled = true;
            lbl8x.Enabled = true;
            lbl9o.Enabled = true;
            lbl9x.Enabled = true;

            lbl1o.Visible = false;
            lbl2o.Visible = false;
            lbl3o.Visible = false;
            lbl4o.Visible = false;
            lbl5o.Visible = false;
            lbl6o.Visible = false;
            lbl7o.Visible = false;
            lbl8o.Visible = false;
            lbl9o.Visible = false;

            lbl1x.Visible = false;
            lbl2x.Visible = false;
            lbl3x.Visible = false;
            lbl4x.Visible = false;
            lbl5x.Visible = false;
            lbl6x.Visible = false;
            lbl7x.Visible = false;
            lbl8x.Visible = false;
            lbl9x.Visible = false;
            gvg.Visible = true;
            gvk.Visible = true;
            zkomp = false;
            wstawieBoWygram = false;
            label12.Text = String.Empty;
            wygranaxzkomp = false;
            wygranaokomputera = false;
        }

        private void czyWygramJakWstawie()
        {

            //12_
            if (label1.Text == "o" && label2.Text == "o" && label3.Text == "")
            { label3.Text = "o"; czyWygrana(); wstawieBoWygram = true; }
            else
            {//1_3
                if (label1.Text == "o" && label3.Text == "o" && label2.Text == "")
                { label2.Text = "o"; czyWygrana(); wstawieBoWygram = true; }
                else
                {//_23
                    if (label2.Text == "o" && label3.Text == "o" && label1.Text == "")
                    { label1.Text = "o"; czyWygrana(); wstawieBoWygram = true; }
                    else
                    {//45_
                        if (label4.Text == "o" && label5.Text == "o" && label6.Text == "")
                        { label6.Text = "o"; czyWygrana(); wstawieBoWygram = true; }
                        else
                        {//_56
                            if (label5.Text == "o" && label6.Text == "o" && label4.Text == "")
                            { label4.Text = "o"; czyWygrana(); wstawieBoWygram = true; }
                            else
                            {//4_6
                                if (label4.Text == "o" && label6.Text == "o" && label5.Text == "")
                                { label5.Text = "o"; czyWygrana(); wstawieBoWygram = true; }
                                else
                                {//78_
                                    if (label7.Text == "o" && label8.Text == "o" && label9.Text == "")
                                    { label9.Text = "o"; czyWygrana(); wstawieBoWygram = true; }
                                    else
                                    {//_89
                                        if (label8.Text == "o" && label9.Text == "o" && label7.Text == "")
                                        { label7.Text = "o"; czyWygrana(); wstawieBoWygram = true; }
                                        else
                                        {//7_9
                                            if (label7.Text == "o" && label9.Text == "o" && label8.Text == "")
                                            { label8.Text = "o"; czyWygrana(); wstawieBoWygram = true; }
                                            else
                                            {//7_3
                                                if (label7.Text == "o" && label3.Text == "o" && label5.Text == "")
                                                { label5.Text = "o"; czyWygrana(); wstawieBoWygram = true; }
                                                else
                                                {//_53
                                                    if (label5.Text == "o" && label3.Text == "o" && label7.Text == "")
                                                    { label8.Text = "o"; czyWygrana(); wstawieBoWygram = true; }
                                                    else
                                                    {//75_
                                                        if (label7.Text == "o" && label5.Text == "o" && label3.Text == "")
                                                        { label3.Text = "o"; czyWygrana(); wstawieBoWygram = true; }
                                                        else
                                                        {//15_
                                                            if (label1.Text == "o" && label5.Text == "o" && label9.Text == "")
                                                            { label9.Text = "o"; czyWygrana(); wstawieBoWygram = true; }
                                                            else
                                                            {//1_9
                                                                if (label1.Text == "o" && label9.Text == "o" && label5.Text == "")
                                                                { label5.Text = "o"; czyWygrana(); wstawieBoWygram = true; }
                                                                else
                                                                {//_59
                                                                    if (label5.Text == "o" && label9.Text == "o" && label1.Text == "")
                                                                    { label1.Text = "o"; czyWygrana(); wstawieBoWygram = true; }
                                                                    else
                                                                    {//14_
                                                                        if (label1.Text == "o" && label4.Text == "o" && label7.Text == "")
                                                                        { label7.Text = "o"; czyWygrana(); wstawieBoWygram = true; }
                                                                        else
                                                                        //1_7
                                                                        {
                                                                            if (label1.Text == "o" && label7.Text == "o" && label4.Text == "")
                                                                            { label4.Text = "o"; czyWygrana(); wstawieBoWygram = true; }
                                                                            else
                                                                            {//_47
                                                                                if (label4.Text == "o" && label7.Text == "o" && label1.Text == "")
                                                                                { label1.Text = "o"; czyWygrana(); wstawieBoWygram = true; }
                                                                                else
                                                                                {//25_
                                                                                    if (label2.Text == "o" && label5.Text == "o" && label8.Text == "")
                                                                                    { label8.Text = "o"; czyWygrana(); wstawieBoWygram = true; }
                                                                                    else
                                                                                    {//2_8
                                                                                        if (label2.Text == "o" && label8.Text == "o" && label5.Text == "")
                                                                                        { label5.Text = "o"; czyWygrana(); wstawieBoWygram = true; }
                                                                                        else
                                                                                        {//_58
                                                                                            if (label5.Text == "o" && label8.Text == "o" && label2.Text == "")
                                                                                            { label2.Text = "o"; czyWygrana(); wstawieBoWygram = true; }
                                                                                            else
                                                                                            {//36_
                                                                                                if (label3.Text == "o" && label6.Text == "o" && label9.Text == "")
                                                                                                { label9.Text = "o"; czyWygrana(); wstawieBoWygram = true; }
                                                                                                else
                                                                                                {//3_9
                                                                                                    if (label3.Text == "o" && label9.Text == "o" && label6.Text == "")
                                                                                                    { label6.Text = "o"; czyWygrana(); wstawieBoWygram = true; }
                                                                                                    else
                                                                                                    {//_69
                                                                                                        if (label6.Text == "o" && label9.Text == "o" && label3.Text == "")
                                                                                                        { label3.Text = "o"; czyWygrana(); wstawieBoWygram = true; }
                                                                                                    }
                                                                                                }
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    
        private void czyPuste()
        {
            if (label1.Text == String.Empty || label2.Text == String.Empty || label3.Text == String.Empty || label4.Text == String.Empty || label5.Text == String.Empty || label6.Text == String.Empty || label7.Text == String.Empty || label8.Text == String.Empty || label9.Text == String.Empty)
            { }
            else
            {
             label12.Text = "Brak wygranej";
                //czyszczeniePlanszy();
            } 
        }
    }
}
